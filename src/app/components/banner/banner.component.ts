import { Component, OnInit } from '@angular/core';
import { DataservService } from  '../../services/dataserv.service'

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  name:string;
  age:number;
  address:Addresses;
  hobbies:any;
  posts:post[]

  constructor(private dataService:DataservService) { }

  ngOnInit() {
    this.name='jyothish';
    this.age=25;
    this.address=
    {
      houseName: 'Kooramplackal',
      email: 'jyothish@accubits.com' ,
      houseNo: 'an123' ,
      phone: 1234567890 ,
    }
    this.hobbies = ['Driving','Movies'];
    this.dataService.getPosts().subscribe((posts)=>
    {
      //console.log(posts);
      this.posts = posts;
    });
  }

  demoClick()
  {
    this.name="click_change";
    this.address.email="aj2accubits.com";
    this.hobbies.push('New Hobby')
  }
  demoEdit()
  {
   this.isEdit = !this.isEdit;
  }
  addHobby(jyoHobby)
  {
    this.hobbies.unshift(jyoHobby);
    return false;
  }
  delHobbies(events)
  {
    for(let i=0;i<this.hobbies.length;i++)
    {
      if(this.hobbies[i] == events){
        this.hobbies.splice(i, 1);
      }
    }
  }
}
interface Addresses {
  houseName:string;
  email:string;
  houseNo:any;
  phone:number;
}
 interface post{
   id:number,
   title:string,
   body:string,
   UserId:number
 }