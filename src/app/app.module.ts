import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { BannerComponent } from './components/banner/banner.component';

import { DataservService } from './services/dataserv.service';
import { AboutComponent } from './components/about/about.component';

const appRoutes: Routes = [
  {path:'', component:BannerComponent},
  {path:'about', component:AboutComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [DataservService],
  bootstrap: [AppComponent]
})
export class AppModule { }
