import { TestBed, inject } from '@angular/core/testing';

import { DataservService } from './dataserv.service';

describe('DataservService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataservService]
    });
  });

  it('should be created', inject([DataservService], (service: DataservService) => {
    expect(service).toBeTruthy();
  }));
});
